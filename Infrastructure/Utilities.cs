﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Infrastructure
{
    public static class Utilities
    {
        public static string GetClientIPAddress(HttpRequest httpRequest)
        {
            string ipAddress = "";
            if (httpRequest.Headers["Client-IP"].Count > 0)
            {
                ipAddress = httpRequest.Headers["Client-IP"].ToString();
            }
            else if (httpRequest.Headers["X-Forwarded-For"].Count > 0)
            {
                if (httpRequest.Headers["X-Forwarded-For"].ToString().Contains(","))
                {
                    ipAddress = httpRequest.Headers["X-Forwarded-For"].ToString();
                    ipAddress = ipAddress.Substring(0, ipAddress.IndexOf(","));
                }
                else
                {
                    ipAddress = httpRequest.Headers["X-Forwarded-For"];
                }
            }
            else
            {
                ipAddress = httpRequest.Host.Host;
            }

            return ipAddress;
        }
    }
}
