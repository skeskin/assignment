﻿using Application.Models;
using Application.Repositories;
using Core.Entities;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class AdvertRepository : IAdvertRepository
    {
        private readonly DapperContext _context;

        public AdvertRepository(DapperContext context)
        {
            this._context = context;
        }

        public async Task<Advert> GetByIdAsync(int id)
        {
            using (var connection = _context.CreateConnection())
            {
                connection.Open();

                var parameters = new DynamicParameters();
                parameters.Add("advertId", id);

                var result = await connection.QuerySingleOrDefaultAsync<Advert>("getAdvertById",parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }
        public async Task CreateAdvertVisitAsync(AdvertVisit advertVisit)
        {
            using (var connection = _context.CreateConnection())
            {
                connection.Open();
                var parameters = new DynamicParameters();
                parameters.Add("advertId", advertVisit.advertId);
                parameters.Add("iPAdress", advertVisit.iPAdress);
                parameters.Add("visitDate", advertVisit.visitDate);

                var result = await connection.ExecuteAsync("createAdvertVisit", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<AdvertPagingDto> QueryAdvertsAsync(int categoryId, string price, string fuel, string gear, int page)
        {
            using (var connection = _context.CreateConnection())
            {
                connection.Open();

                var parameters = new DynamicParameters();
                parameters.Add("categoryId", categoryId);
                parameters.Add("price", price);
                parameters.Add("fuel", fuel);
                parameters.Add("gear", gear);
                parameters.Add("page", page);
                parameters.Add("totalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);
                var adverts = await connection.QueryAsync<AdvertDto>("queryAdverts", parameters, commandType: CommandType.StoredProcedure);
                int totalCount = parameters.Get<int>("totalCount");

                AdvertPagingDto data = new AdvertPagingDto()
                {
                    adverts = adverts.ToList(),
                    page = page,
                    total = totalCount
                };

                return data;
            }
        }
    }
}
