﻿using Application.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IAdvertRepository advertRepository) {
            this.Adverts = advertRepository;
        }
        public IAdvertRepository Adverts { get; }
    }
}
