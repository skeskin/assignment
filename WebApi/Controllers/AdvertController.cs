﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Repositories;
using Core.Entities;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    /// <summary>    
    /// Vehicle Adverts Api    
    /// </summary> 
    [ApiController]
    [Route("advert")]
    public class AdvertController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public AdvertController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        /// <summary>    
        /// Query adverts    
        /// </summary>    
        /// <returns></returns>
        /// <response code="200">Successful operation</response>
        /// <response code="204">No advert found</response>
        /// <response code="500">Internal error occured</response> 
        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> QueryAdverts(int categoryId, string price, string fuel, string gear, int page)
        {
            var data = await unitOfWork.Adverts.QueryAdvertsAsync(categoryId, price, fuel, gear, page);

            if (data.adverts.Count > 0)
            {
                return Ok(data);
            }
            else
            {
                return StatusCode(204);
            }
        }

        /// <summary>    
        /// Record advert visit
        /// </summary>    
        /// <returns></returns> 
        /// <response code="201">Visit created</response>    
        /// <response code="500">Internal error occured</response>  
        [Route("visit")]
        [HttpPost]
        public async Task<IActionResult> RecordAdvertVisit(int advertId)
        {
            AdvertVisit advertVisit = new AdvertVisit()
            {
                advertId = advertId,
                visitDate = DateTime.Now,
                iPAdress = Utilities.GetClientIPAddress(Request)
            };

            await unitOfWork.Adverts.CreateAdvertVisitAsync(advertVisit);
            return StatusCode(201);
        }

        /// <summary>    
        /// Get advert by id
        /// </summary>    
        /// <returns></returns> 
        /// <response code="200">Successful operation</response>
        /// <response code="204">No advert found</response>
        /// <response code="500">Internal error occured</response>  
        [HttpGet("get/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var data = await unitOfWork.Adverts.GetByIdAsync(id);
            if (data == null)
            {
                return StatusCode(204);
            }
            else
            {
                return Ok(data);
            }
        }
    }
}