﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class AdvertVisit
    {
        public int advertId { get; set; }
        public string iPAdress { get; set; }
        public DateTime visitDate { get; set; }
    }
}
