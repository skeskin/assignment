﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Models
{
    public class AdvertPagingDto
    {
        public List<AdvertDto> adverts { get; set; }
        public int total { get; set; }
        public int page { get; set; }
    }
}
