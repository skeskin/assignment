﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Application.Models
{
    public class AdvertDto
    {
        public int id { get; set; }
        public string modelName { get; set; }
        public int year { get; set; }
        public decimal price { get; set; }
        public string title { get; set; }
        public DateTime date { get; set; }
        public string category { get; set; }
        public int km { get; set; }
        public string color { get; set; }
        public string gear { get; set; }
        public string fuel { get; set; }
        public string firstPhoto { get; set; }
    }
}
