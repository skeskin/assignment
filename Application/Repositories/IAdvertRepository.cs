﻿using Application.Models;
using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IAdvertRepository : IGenericRepository<Advert>
    {
        Task<AdvertPagingDto> QueryAdvertsAsync(int categoryId, string price, string fuel, string gear, int page);

        Task CreateAdvertVisitAsync(AdvertVisit advertVisit);
    }
}
